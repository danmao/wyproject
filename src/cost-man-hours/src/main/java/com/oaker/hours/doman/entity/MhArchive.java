package com.oaker.hours.doman.entity;


import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;

@Data
@Accessors(chain = true)
@TableName("mh_archive")
public class MhArchive  implements Serializable {

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /** 部门id */
    @TableField(value = "dept_id")
    private Long deptId;

    /** 归档月份 */
    @TableField(value = "archive_date")
    private String archiveDate;


    /** 创建时间 */
    @TableField(value = "create_time")
    private Date createTime;

    /** 创建人 */
    @TableField(value = "create_user")
    private long createUser;

}
